/**
 * Created by ginollerena on 4/24/18.
 */

import Vector from './Vector'
import Line, {is_near_zero, first_nonzero_index} from './Line'

const NO_NONZERO_ELTS_FOUND_MSG = 'No nonzero elements found';


export default class Plane {
  constructor( normal_vector=null, constant_term=null){
    this.dimension = 3

    if (!normal_vector) {
      const allzeros = new Array(this.dimension).fill(0);
      this.normal_vector = new Vector(allzeros);
    }else{
      this.normal_vector = normal_vector;
    }

    this.constant_term = !constant_term ? 0 : constant_term;
    this.set_basepoint();
  }

  set_basepoint(){
    try{

      const n = this.normal_vector
      const c = this.constant_term
      const basepoint_coords = new Array(this.dimension).fill(0);

      const initial_index = first_nonzero_index(n.getVector());
      const initial_coefficient = n.getVector().get[initial_index]

      basepoint_coords[initial_index] = c/initial_coefficient
      this.basepoint = new Vector(basepoint_coords)

    }
    catch(ex){
      throw NO_NONZERO_ELTS_FOUND_MSG;
    }
  }

  is_parallel(plane2) {
    return this.normal_vector.is_parallel(plane2.normal_vector);
  }

  isEqual(plane2) {
    if (this.normal_vector.is_Zero()) {
      if (!plane2.normal_vector.is_Zero())
        return false;

      const diff = this.constant_term - plane2.constant_term;
      return is_near_zero(diff);

    }
    else if (plane2.normal_vector.is_Zero())
      return false;

    if (!this.is_parallel(plane2))
      return false;

    const basepoint_difference = this.basepoint.minus(plane2.basepoint);
    return basepoint_difference.isOrthogonal(this.normal_vector);
  }

  hasEqualValues(plane){
    return this.normal_vector.hasEqualValues(plane.normal_vector);
  }
}