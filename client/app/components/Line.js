/**
 * Created by ginollerena on 4/20/18.
 */

import {List, isImmutable} from 'immutable'
import Vector from './Vector'

const eps = 1e-10; //Number.MIN_VALUE

export function is_near_zero(decimal){
  return Math.abs(decimal) <  eps;
}

export function first_nonzero_index(vector) {
  if(vector){
    return vector.findIndex((item)=> {
      return !is_near_zero(item);
    });
  }
  return -1;
}

export default class Line {
  constructor(normal_vector, constant_term) {
    this.dimension = 2;
    if (!normal_vector) {
      const allzeros = new Array(this.dimension).fill(0);
      this.normal_vector = new Vector(allzeros);
    } else {
      this.normal_vector = normal_vector;
    }

    this.constant_term = !constant_term ? 0 : constant_term;
    this.set_basepoint();
  }


  set_basepoint() {

    const n = this.normal_vector;
    const c = this.constant_term;
    const basepoint_coords = new Array(this.dimension).fill(0);

    const initial_index = first_nonzero_index(n.getVector());
    if(initial_index !== -1) {
      const initial_coefficient = n.getVector().get(initial_index);
      basepoint_coords[initial_index] = c / initial_coefficient;
      this.basepoint = new Vector(basepoint_coords);
    }else{
      throw('No nonzero elements found');
    }

  }

  is_parallel(line2) {
    return this.normal_vector.is_parallel(line2.normal_vector);
  }

  isEqual(line2) {
    if (this.normal_vector.is_Zero()) {
      if (!line2.normal_vector.is_Zero())
        return false;
      else {
        const diff = this.constant_term - line2.constant_term;
        return is_near_zero(diff);
      }
    }
    else if (line2.normal_vector.is_Zero())
      return false;

    if (!this.is_parallel(line2))
      return false;

    const basepoint_difference = this.basepoint.minus(line2.basepoint);
    return basepoint_difference.isOrthogonal(this.normal_vector);
  }

  intersection(line2) {

    const [a, b] = this.normal_vector.getCoordinates();
    const [c, d] = line2.normal_vector.getCoordinates();
    const k1 = this.constant_term;
    const k2 = line2.constant_term;
    const denom = ((a * d) - (b * c))

    if (is_near_zero(denom)) {
      if (this.isEqual(line2))
        return this;
      else
        return null;
    }

    const one_over_denom = 1 / ((a * d) - (b * c));
    const x_num = (d * k1 - b * k2);
    const y_num = (-c * k1 + a * k2);

    return new Vector([x_num, y_num]).times_scalar(one_over_denom);
  }
}