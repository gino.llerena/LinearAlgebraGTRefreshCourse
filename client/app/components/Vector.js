/**
 * Created by ginollerena on 4/17/18.
 */
import {List, isImmutable} from 'immutable'

const eps = 1e-10; //Number.MIN_VALUE

export default class Vector {
  constructor(input){
    this.vector = isImmutable(input) ? input : List(input);
  }

  getCoordinates(){
    return this.vector.toJS();
  }

  getVector(){
    return this.vector;
  }

  getMagnitude(){
    return Math.sqrt(this.vector.reduce((mgn,item)=>{
      mgn = mgn + Math.pow(item,2);
      return mgn;
    },0));
  }

  getUnitVector(){
    const mgn = this.getMagnitude();
    return new Vector(this.vector.map((n)=> n/mgn));
  }

  normalized(){
    const mgn = this.getMagnitude();
    return  this.times_scalar(1.0/mgn);
  }

  suma(vector){
    const zip = this.vector.zip(vector.getVector());
    return new Vector(zip.map((item)=> item.reduce((acc,value) =>{acc+=value; return acc}, 0)));
  }

  multiplyVectors(vector){
    const zip = this.vector.zip(vector.getVector());
    const result = zip.map((item)=> item.reduce((acc,value, i) => {i === 0 ? acc=value : acc*=value; return acc}, 0));
    return new Vector(result);
  }

  hasEqualValues(vector){
    const value = this.vector.reduce((acc,item,i)=>{
      acc = acc + Math.abs(item - vector.getVector().get(i))
      return acc;
    },0)

    return (value === 0);
  }

  multiply(vector){
    const zip = this.vector.zip(vector.getVector());
    const result = zip.map((item)=> item.reduce((acc,value, i) => {i === 0 ? acc=value : acc*=value; return acc}, 0));
    return result.reduce((acc, item)=>{acc+=item; return acc;}, 0);
  }

  minus(vector){
    const zip = this.vector.zip(vector.getVector());
    return new Vector(zip.map((item)=> item.reduce((acc,value, i) =>{i === 0 ? acc=value : acc-=value; return acc}, 0)));
  }

  times_scalar(value){
    return new Vector(this.vector.map((n)=> n*value));
  }

  angle(vector, grados = false){
    const v1w1 = this.multiply(vector);
    const mv1 = vector.getMagnitude();
    const mw1 = this.getMagnitude();

    const rad = Math.acos(v1w1/(mv1*mw1));

    if(grados)
      return (180*rad)/Math.PI;
    else
      return rad;
  }

  isOrthogonal(vector){
    const result = this.multiply(vector);
    return (result <= eps);
  }

  is_Zero(){
    return this.getMagnitude() < eps;
  }

  is_parallel(vector){
    /*const rad = this.angle(vector);
    if(isNaN(rad) || this.is_Zero() || vector.is_Zero() ||  rad <= eps || Math.abs(rad - Math.PI) <= eps)
      return true;
    else
      return false;*/

    return (Math.abs(Math.round(this.multiply(vector))) === Math.round(this.getMagnitude()*vector.getMagnitude(), 2))

  }

  getProjectionOf(vector){
    return vector.normalized().times_scalar(this.multiply(vector.normalized()));
  }

  component_parallel_to(vector){
    const u = vector.normalized();
    const weight = this.multiply(u);
    return u.times_scalar(weight);
  }

  component_orthogonal_to(vector){
    const projection = this.component_parallel_to(vector);
    return this.minus(projection);
  }

  cross(vector){
    if(vector.getVector().size === 3 && this.getVector().size === 3){
        const v = this.getVector();
        const w = vector.getVector();
        return new Vector([ v.get(1)*w.get(2) - v.get(2)*w.get(1),  -(v.get(0)*w.get(2) - v.get(2)*w.get(0)), v.get(0)*w.get(1) - v.get(1)*w.get(0)])
    }
  }

  area_of_parallelogram_with(vector){
    const cross_product= this.cross(vector);
    return cross_product.getMagnitude();

  }

  area_of_triangle_with(vector){
    return this.area_of_parallelogram_with(vector) / 2;
  }

}