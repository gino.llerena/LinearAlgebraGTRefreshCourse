/**
 * Created by ginollerena on 4/27/18.
 */

import {List, isImmutable} from 'immutable'
import Line, {is_near_zero, first_nonzero_index} from './Line'
import Plane from './Plane'
import Vector from './Vector'
import Parametrization from './Parametrization'
import difference from 'lodash/difference'
import range from 'lodash/range'
import some from 'lodash/some'

const ALL_PLANES_MUST_BE_IN_SAME_DIM_MSG = 'All planes in the system should live in the same dimension';
const NO_SOLUTIONS_MSG = 'No solutions';
const INF_SOLUTIONS_MSG = 'Infinitely many solutions';

export default class LinearSystem{
  constructor(planes){
    try {
      const d = planes[0].dimension;
      for(let i=0; i<planes.length; i++) {
        if(planes[i].dimension !== d){
          throw new Error(ALL_PLANES_MUST_BE_IN_SAME_DIM_MSG);
          break;
        }
      }

      this.planes = List(planes);
      this.dimension = d;
    }
    catch(e) {
      console.log(e.message);
    }
  }

   swap_rows(row1, row2){
     const plane = this.planes.get(row1);
     this.planes = this.planes.set(row1, this.planes.get(row2));
     this.planes = this.planes.set(row2, plane);
   }

  multiply_coefficient_and_row(coefficient, row){
    const plane = this.planes.get(row);
    const normal_vector = plane.normal_vector.times_scalar(coefficient);
    this.planes = this.planes.set(row, new Plane(normal_vector));
  }


   add_multiple_times_row_to_row(coefficient, row_to_add, row_to_be_added_to){
     const vector = this.planes.get(row_to_add).normal_vector.getVector().zip(this.planes.get(row_to_be_added_to).normal_vector.getVector())
     const newVector = vector.reduce((acc,item)=>{
       const x=item[0];
       const y=item[1];
       acc.push(x*coefficient + y);
       return acc;
     },[])

     const constant_term=(this.planes.get(row_to_be_added_to).constant_term + (this.planes.get(row_to_add).constant_term*coefficient));
     this.setItem(row_to_be_added_to, new Plane(new Vector(newVector), constant_term));
  }


  indices_of_first_nonzero_terms_in_each_row() {
    const num_equations = this.length();
    const num_variables = this.dimension

    let indices = new Array(num_equations).fill(-1);

    this.planes.forEach((p,i)=>{
        const normal_vector = p.normal_vector;
        indices[i] = first_nonzero_index(normal_vector.getVector());
    })

    //if str(e) == Plane.NO_NONZERO_ELTS_FOUND_MSG:

    return indices
  }

  compute_triangular_form(){
    const system = this;
    const num_equations = system.length();
    const num_variables = system.dimension;


    system.planes.forEach((plane, current_row)=>{
      system.check_for_swaps(current_row);
      let indices = system.indices_of_first_nonzero_terms_in_each_row()
      let first_term_pos = indices[current_row];

      while(first_term_pos < current_row && first_term_pos !== -1)
      {
        const coordinates1 = system.getItem(current_row).normal_vector.getCoordinates();
        const coordinates2 = system.getItem(first_term_pos).normal_vector.getCoordinates();
        const coef = -coordinates1[first_term_pos] / coordinates2[first_term_pos];
        system.add_multiple_times_row_to_row(coef, first_term_pos, current_row)
        indices = system.indices_of_first_nonzero_terms_in_each_row();
        if (first_term_pos === indices[current_row])
          break;
        else
          first_term_pos = indices[current_row]
      }
    })

    return system;

  }

   check_for_swaps(row) {

     const indices = this.indices_of_first_nonzero_terms_in_each_row();

     if (indices[row] > row) {
       for(let i=0; i<indices.length; i++){
         if(indices[i] <= row) {
           this.swap_rows(row, i);
           break;
         }
       }
     }
   }

   compute_rref() {
     const tf = this.compute_triangular_form();

     let loop_num = 0
     if (tf.getItem(0).normal_vector.getCoordinates().length > tf.length())
      loop_num = tf.length();
     else
      loop_num = tf.getItem(0).normal_vector.getCoordinates().length;

     for(let current_row=0; current_row<loop_num; current_row++){
       //const indices = tf.indices_of_first_nonzero_terms_in_each_row();
       //const first_term_pos = indices[current_row];

       if (tf.getItem(current_row).normal_vector.getVector().get(current_row) !== 1 && tf.getItem(current_row).normal_vector.getVector().get(current_row) !== 0){
         const coef = (1.0/tf.getItem(current_row).normal_vector.getCoordinates()[current_row]);
         tf.multiply_coefficient_and_row(coef, current_row);
       }

       let i = current_row + 1;
       while (i < tf.getItem(current_row).normal_vector.getCoordinates().length) {
         if ((tf.getItem(current_row).normal_vector.getCoordinates()[i] !== 0)
            && (tf.length() > i)
            && (tf.getItem(i).normal_vector.getCoordinates()[i] !== 0)){
           const coef = -tf.getItem(current_row).normal_vector.getCoordinates()[i] / tf.getItem(i).normal_vector.getCoordinates()[i];
           tf.add_multiple_times_row_to_row(coef, i, current_row)
         }
         i += 1;
       }

     }

     return tf;
   }

  compute_rref1(self) {
    const tf = this.compute_triangular_form()

    const num_equations = tf.length();
    const pivot_indices = tf.indices_of_first_nonzero_terms_in_each_row();

    const equations_list = range(num_equations).reverse();

    /*for row in range(num_equations)[:: - 1]:
    pivot_var = pivot_indices[row]
    if pivot_var < 0:
    continue
    tf.scale_row_to_make_coefficient_equal_one(row, pivot_var)
    tf.clear_coefficients_above(row, pivot_var)*/

    return tf;
  }

   raise_exception_if_contradictory_equation() {
       for(const plane of this.planes){
         const idx = first_nonzero_index(plane.normal_vector.getVector());
         if(idx === -1){
           throw new Error(NO_SOLUTIONS_MSG);
           break;
         }
       }
   }

  raise_exception_if_too_few_pivots() {
      const pivot_indices = this.indices_of_first_nonzero_terms_in_each_row();
      const num_variables = this.dimension;
      let num_pivots = 0;

      for (let index in pivot_indices) {
        num_pivots = num_pivots + ((index >= 0) ? 1 : 0);
      }

      if (num_pivots < num_variables) {
        throw new Error(INF_SOLUTIONS_MSG);
      }

  }

  extract_direction_vectors_for_parametrization() {
    const num_variables = this.dimension;
    const pivot_indices = this.indices_of_first_nonzero_terms_in_each_row()
    const free_variable_indices = difference(range(num_variables),pivot_indices);

    const direction_vectors = List();

    free_variable_indices.forEach((free_var)=>{
      let vector_coords =  new Array(num_variables).fill(0);
      vector_coords[free_var] = 1;
      let index = 0;
      for(const plane of this.planes){
        const  pivot_var = pivot_indices[index];
        if (pivot_var < 0){
          break;
        }
        vector_coords[pivot_var] = -plane.normal_vector.getVector().get(free_var);
        index++;
      }
      direction_vectors.push(new Vector(vector_coords));
    })

    return direction_vectors;
  }

  extract_basepoint_for_parametrization() {
    const num_variables = this.dimension;
    const pivot_indices = this.indices_of_first_nonzero_terms_in_each_row();

    const basepoint_coords = new Array(num_variables).fill(0);

    let index = 0;
    for(const plane of this.planes){
      const pivot_var = pivot_indices[index]
      if (pivot_var < 0)
        break;
      basepoint_coords[pivot_var] = plane.constant_term;
      index++;
    }

    return new Vector(basepoint_coords);
  }

   do_gaussian_elimination(self) {
     const rref = this.compute_rref()
     let solution_coordinates = [];
     try {
       rref.raise_exception_if_contradictory_equation();
       rref.raise_exception_if_too_few_pivots();
       const num_variables = rref.dimension;
       const list = range(num_variables);

       for(let i in list){
         const plane = this.getItem(i);
         solution_coordinates.push(plane.constant_term);
       }

     }catch(e){
        console.log(e.message);
     }
     return (new Vector(solution_coordinates));
   }


  do_gaussian_elimination_and_parametrization() {
    const rref = this.compute_rref();
    rref.raise_exception_if_contradictory_equation();

    const direction_vectors = rref.extract_direction_vectors_for_parametrization() ;
    const basepoint = rref.extract_basepoint_for_parametrization();

    return new Parametrization(basepoint, direction_vectors)
  }

  compute_solution() {
    try{
      return this.do_gaussian_elimination_and_parametrization();
    }catch(e){
      console.log(e.message);
    }
  }

  length() {
     return this.planes.size;
   }


  getItem(i){
    return this.planes.get(i);
  }

  setItem(i, x) {
    try {
      if( x.dimension === this.dimension)
        this.planes = this.planes.set(i,x);
      else
         throw new Error(self.ALL_PLANES_MUST_BE_IN_SAME_DIM_MSG)

    }
    catch(e) {
        console.log(e.message);
    }
  }


}