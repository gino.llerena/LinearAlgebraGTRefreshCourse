/**
 * Created by ginollerena on 5/1/18.
 */

const BASEPT_AND_DIR_VECTORS_MUST_BE_IN_SAME_DIM = 'The basepoint and direction vectors should all live in the same dimension';

export default class Parametrization{
  constructor(basepoint, direction_vectors){
    this.basepoint = basepoint;
    this.direction_vectors = direction_vectors;
    this.dimension = this.basepoint.dimension;

    try{
      for(let v in direction_vectors){
        if(v.dimension !== this.dimension){
          throw(BASEPT_AND_DIR_VECTORS_MUST_BE_IN_SAME_DIM);
        }
      }
    }catch(e){
      console.log(e.message);
    }
  }


}