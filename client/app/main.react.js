
import React from 'react'
import ReactDOM from 'react-dom'

import Vector from './components/Vector'
import Line from './components/Line'
import Plane from './components/Plane'
import LinearSystem from './components/LinearSystem'


//const v = new Vector([3.183, -7.627]);
//const w = new Vector([-2.668, 5.319]);

//const v = new Vector([1,2,-1]);
//const w = new Vector([3,1,0]);

//const v = new Vector([-7.579,-7.88]);
//const w = new Vector([22.737,23.64]);

const v = new Vector([3.039,1.879]);
const w = new Vector([0.825,2.036]);



console.log('Magnitude',v.getMagnitude());
console.log('UnitVector',v.getUnitVector());
console.log('Suma',v.suma(w));
console.log('Multiplica',v.multiply(w));
console.log('Angle RAD',v.angle(w));
console.log('Angle Grados',v.angle(w, true));
console.log('isOrthogonal',v.isOrthogonal(w));
console.log('isParallel',v.is_parallel(w));
console.log('Projection',v.component_parallel_to(w));
console.log('Orthogonal',v.component_orthogonal_to(w));

const v12_1 = new Vector([-9.88,-3.264, -8.159]);
const b12_1 = new Vector([-2.155,-9.353,-9.473]);

console.log('Projection',v12_1.component_parallel_to(b12_1));
console.log('Orthogonal',v12_1.component_orthogonal_to(b12_1));

const v14_1 = new Vector([8.462,7.893, -8.187]);
const w14_1 = new Vector([6.984,-5.975,4.778]);

const v14_2 = new Vector([-8.987,-9.838, 5.031]);
const w14_2 = new Vector([-4.268,-1.861,-8.866]);

console.log('Product',v14_1.cross(w14_1));
console.log('Area',v14_2.area_of_parallelogram_with(w14_2));

// first system
// 4.046x + 2.836y = 1.21
// //10.115x + 7.09y = 3.025

const line1 = new Line(new Vector([4.046, 2.836]), 1.21);
const line2 = new Line(new Vector([10.115, 7.09]), 3.025);

console.log('first system instersects in:',(line1.intersection(line2)));
console.log('first system is parallel:',(line1.is_parallel(line2)));
console.log('first system is Equal:',(line1.isEqual(line2)));


// second system
// 7.204x + 3.182y = 8.68
// 8.172x + 4.114y = 9.883

const line3 = new Line(new Vector([7.204, 3.182]), 8.68)
const line4 = new Line(new Vector([8.172, 4.114]), 9.883)

console.log(`second system instersects in:`, line3.intersection(line4));
console.log('first system is parallel:',(line3.is_parallel(line4)));
console.log('first system is Equal:',(line3.isEqual(line4)));

// third system
// 1.182x + 5.562y = 6.744
// 1.773x + 8.343y = 9.525

const line5 = new Line(new Vector([1.182, 5.562]), 6.744)
const line6 = new Line(new Vector([1.773, 8.343]), 9.525)

console.log(`third system instersects in:`, line5.intersection(line6));
console.log('first system is parallel:',(line5.is_parallel(line6)));
console.log('first system is Equal:',(line5.isEqual(line6)));

//// Plane
// first system of planes:
// -0.412x + 3.806y + 0.728z = -3.46
// 1.03x - 9.515y - 1.82z = 8.65

const plane1 = new Plane(new Vector([-0.412, 3.806, 0.728]), -3.46)
const plane2 = new Plane(new Vector([1.03, -9.515, -1.82]), 8.65)

console.log('1 is parallel: {}', plane1.is_parallel(plane2));
console.log('1 is equal: {}', plane1.isEqual(plane2));

// second system of planes:
// 2.611x + 5.528y + 0.283z = 4.6
// 7.715x + 8.306y + 5.342z = 3.76

const plane3 = new Plane(new Vector([2.611, 5.528, 0.283]), 4.6)
const plane4 = new Plane(new Vector([7.715, 8.306, 5.342]), 3.76)

console.log('2 is parallel: {}',plane3.is_parallel(plane4));
console.log('2 is equal: {}', plane3.isEqual(plane4));

// third system of planes:
// -7.926x + 8.625y - 7.212z = -7.952
// -2.642x + 2.875y - 2.404z = -2.443

const plane5 = new Plane(new Vector([-7.926, 8.625, -7.212]), -7.952);
const plane6 = new Plane(new Vector([-2.642, 2.875, -2.404]), -2.443);

console.log('3 is parallel: {}', plane5.is_parallel(plane6));
console.log('3 is equal: {}', plane5.isEqual(plane6));

//////Extra

const ex_1 = new Vector([3,4]);
const ex_2 = new Vector([5,6]);

console.log('Projection',ex_1.getProjectionOf(ex_2));




const p0 = new Plane(new Vector([1,1,1]), 1)
const p1 = new Plane(new Vector([0,1,0]), 2)
const p2 = new Plane(new Vector([1,1,-1]), 3)
const p3 = new Plane(new Vector([1,0,-2]), 2)

const s = new LinearSystem([p0,p1,p2,p3])

console.log(s.indices_of_first_nonzero_terms_in_each_row());
// print '{},{},{},{}'.format(s[0],s[1],s[2],s[3])
// print len(s)
// print s




s.swap_rows(0, 1)
if (!(s.getItem(0) ===p1 && s.getItem(1) === p0 && s.getItem(2) === p2 && s.getItem(3) === p3))
  console.log('test case 1 failed');

s.swap_rows(1, 3)
if  (!(s.getItem(0) === p1 && s.getItem(1) === p3 && s.getItem(2) === p2 && s.getItem(3) === p0))
  console.log('test case 2 failed');

s.swap_rows(3, 1)
if  (!(s.getItem(0) === p1 && s.getItem(1) === p0 && s.getItem(2) === p2 && s.getItem(3) === p3))
  console.log('test case 3 failed');


s.multiply_coefficient_and_row(1, 0)
if (!(s.getItem(0).hasEqualValues(p1) &&
    s.getItem(1).hasEqualValues(p0) &&
    s.getItem(2).hasEqualValues(p2) &&
    s.getItem(3).hasEqualValues(p3)))
  console.log('test case 4 failed');


s.multiply_coefficient_and_row(-1, 2)
const new_s2 = new Plane(new Vector([-1, -1, 1]), -3);
/*
if (!(s[0] === p1 && s[1] === p0 && s[2] === new_s2 && s[3] === p3))
  console.log('test case 5 failed');
*/
s.multiply_coefficient_and_row(10, 1)
const new_s1 = new Plane(new Vector([10, 10, 10]), 10)
/*
if  (!(s[0] === p1 && s[1] === new_s1 && s[2] === new_s2 && s[3] === p3))
  console.log('test case 6 failed')

*/

s.add_multiple_times_row_to_row(0, 0, 1)
if (!(s.getItem(0).hasEqualValues(p1) &&
    s.getItem(1).hasEqualValues(new_s1) &&
    s.getItem(2).hasEqualValues(new_s2) &&
    s.getItem(3).hasEqualValues(p3)))
  console.log('test case 7 failed')

/*
s.add_multiple_times_row_to_row(1, 0, 1)
const added_s1 = Plane(new Vector([10, 11, 10]), 12)
if (!(s[0] === p1 && s[1] === added_s1 && s[2] === new_s2 && s[3] === p3))
  console.log('test case 8 failed')

s.add_multiple_times_row_to_row(-1, 1, 0)
const new_s0 = Plane(new Vector([-10, -10, -10]), -10)
if (!(s[0] === new_s0 && s[1] === added_s1 && s[2] === new_s2 && s[3] === p3))
  console.log('test case 9 failed')

  */


/**************************************************/

const p20ej1_1 = new Plane(new Vector([1, 1, 1]), 1)
const p20ej1_2 = new Plane(new Vector([0, 1, 1]), 2)
const s20ej1 = new LinearSystem([p20ej1_1, p20ej1_2])
const t20ej1 = s20ej1.compute_triangular_form();
if (!(t20ej1.getItem(0).hasEqualValues(p20ej1_1) && t20ej1.getItem(1).hasEqualValues(p20ej1_2)))
  console.log('compute_triangular_form: test case 1 failed');

const p20ej2_1 = new Plane(new Vector([1, 1, 1]), 1)
const p20ej2_2 = new Plane(new Vector([1, 1, 1]), 2)
const s20ej2 = new LinearSystem([p20ej2_1, p20ej2_2])
const t20ej2 = s20ej2.compute_triangular_form()
if  (!(t20ej2.getItem(0).hasEqualValues(p20ej2_1) && t20ej2.getItem(1).hasEqualValues(new Plane(new Vector([0, 0, 0]),1))))
  console.log('compute_triangular_form: test case 2 failed');

const p20ej3_1 = new Plane(new Vector([1, 1, 1]), 1)
const p20ej3_2 = new Plane(new Vector([0, 1, 0]), 2)
const p20ej3_3 = new Plane(new Vector([1, 1, -1]), 3)
const p20ej3_4 = new Plane(new Vector([1, 0, -2]), 2)
const s20ej3 = new LinearSystem([p20ej3_1, p20ej3_2, p20ej3_3, p20ej3_4])
const t20ej3 = s20ej3.compute_triangular_form();
if (!(t20ej3.getItem(0).hasEqualValues(p20ej3_1) &&
      t20ej3.getItem(1).hasEqualValues(p20ej3_2) &&
      t20ej3.getItem(2).hasEqualValues(new Plane(new Vector([0, 0, -2]), 2)) &&
      t20ej3.getItem(3).hasEqualValues(new Plane())))
  console.log('compute_triangular_form: test case 3 failed');

const p20ej4_1 = new Plane(new Vector([0, 1, 1]), 1)
const p20ej4_2 = new Plane(new Vector([1, -1, 1]), 2)
const p20ej4_3 = new Plane(new Vector([1, 2, -5]), 3)
const s20ej4 = new LinearSystem([p20ej4_1, p20ej4_2, p20ej4_3])
const t20ej4 = s20ej4.compute_triangular_form()
if (!(t20ej4.getItem(0).hasEqualValues(new Plane(new Vector([1, -1, 1]), 2)) &&
    t20ej4.getItem(1).hasEqualValues(new Plane(new Vector([0, 1, 1]), 1)) &&
    t20ej4.getItem(2).hasEqualValues(new Plane(new Vector([0, 0, -9]), -2))))
  console.log('compute_triangular_form: test case 4 failed');



/**************************************************/

const p21ej1_1 = new Plane(new Vector([1, 1, 1]), 1)
const p21ej1_2 = new Plane(new Vector([0, 1, 1]), 2)
const s21ej1 = new LinearSystem([p21ej1_1, p20ej1_2])
const t21ej1 = s21ej1.compute_rref();
if (!(t21ej1.getItem(0).hasEqualValues(new Plane(new Vector([1, 0, 0]), -1)) && t21ej1.getItem(1).hasEqualValues(p21ej1_2)))
  console.log('compute_rref: test case 1 failed');

const p21ej2_1 = new Plane(new Vector([1, 1, 1]), 1)
const p21ej2_2 = new Plane(new Vector([1, 1, 1]), 2)
const s21ej2 = new LinearSystem([p21ej2_1, p21ej2_2])
const t21ej2 = s21ej2.compute_rref()
if  (!(t21ej2.getItem(0).hasEqualValues(p21ej2_1) && t21ej2.getItem(1).hasEqualValues(new Plane(new Vector([0, 0, 0]),1))))
  console.log('compute_rref: test case 2 failed');

const p21ej3_1 = new Plane(new Vector([1, 1, 1]), 1)
const p21ej3_2 = new Plane(new Vector([0, 1, 0]), 2)
const p21ej3_3 = new Plane(new Vector([1, 1, -1]), 3)
const p21ej3_4 = new Plane(new Vector([1, 0, -2]), 2)
const s21ej3 = new LinearSystem([p21ej3_1, p21ej3_2, p21ej3_3, p21ej3_4])
const t21ej3 = s21ej3.compute_rref();
if (!(t21ej3.getItem(0).hasEqualValues(new Plane(new Vector([1, 0, 0]), 0)) &&
    t21ej3.getItem(1).hasEqualValues(p21ej3_2) &&
    t21ej3.getItem(2).hasEqualValues(new Plane(new Vector([0, 0, -2]), 2)) &&
    t21ej3.getItem(3).hasEqualValues(new Plane())))
  console.log('compute_rref: test case 3 failed');

const p21ej4_1 = new Plane(new Vector([0, 1, 1]), 1)
const p21ej4_2 = new Plane(new Vector([1, -1, 1]), 2)
const p21ej4_3 = new Plane(new Vector([1, 2, -5]), 3)
const s21ej4 = new LinearSystem([p21ej4_1, p21ej4_2, p21ej4_3])
const t21ej4 = s21ej4.compute_rref()
if (!(t21ej4.getItem(0).hasEqualValues(new Plane(new Vector([1, 0, 0]), 23/9)) &&
    t21ej4.getItem(1).hasEqualValues(new Plane(new Vector([0, 1, 0]), 7/9)) &&
    t21ej4.getItem(2).hasEqualValues(new Plane(new Vector([0, 0, 1]), -2/9))))
  console.log('compute_rref: test case 4 failed');

/**************************************************/
const p3ej1_1 = new Plane(new Vector([5.862,1.178,-10.366]), -8.15)
const p3ej1_2 = new Plane(new Vector([-2.931,-.589,5.183]), -4.075)
// p3 = Plane(normal_vector=Vector([1,2,-5]), constant_term=3)
const s3ej1 = new LinearSystem([p3ej1_1,p3ej1_2])
console.log('Test 1', s3ej1)
console.log('do_gaussian_elimination 1', s3ej1.do_gaussian_elimination());

const p3ej2_1 = new Plane(new Vector([8.631,5.112,-1.816]), -5.113)
const p3ej2_2 = new Plane(new Vector([4.315,11.132,-5.27]), -6.775)
const p3ej2_3 = new Plane(new Vector([-2.158,3.01,-1.727]), -0.831)
const s3ej2 = new LinearSystem([p3ej2_1,p3ej2_2,p3ej2_3])
console.log('Test 2', s3ej2)
console.log('do_gaussian_elimination 2', s3ej2.do_gaussian_elimination());

const p3ej3_1 = new Plane(new Vector([5.262,2.739,-9.878]), -3.441)
const p3ej3_2 = new Plane(new Vector([5.111,6.358,7.638]), -2.152)
const p3ej3_3 = new Plane(new Vector([2.016,-9.924,-1.367]), -9.278)
const p3ej3_4 = new Plane(new Vector([2.167,-13.593,-18.883]), -10.567)
const s3ej3 = new LinearSystem([p3ej3_1,p3ej3_2,p3ej3_3,p3ej3_4]);
console.log('Test 3',s3ej3);
console.log('do_gaussian_elimination 3', s3ej3.do_gaussian_elimination());

ReactDOM.render(<div>Hola</div>, document.getElementById('app-container'));